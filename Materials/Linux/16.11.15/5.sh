#!/bin/bash
var=0
while [ $var -ne 10 ]
do
	for i in $*
	do
		if [ $i -ne $var ]
		then
			touch $var
		else
			echo #!/bin/bash > $var
		fi
	done
	let "var+=1"
done
