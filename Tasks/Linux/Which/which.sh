#!/bin/bash
found=0
check()
{
	if [[ -x $1 ]]
	then
	  found=$1
	fi
}

if [[ "$1" == "" ]] 
then
	echo "Wrong number of arguments" && exit
fi
IFS=$':'
paths=$PWD":"$PATH
for i in $paths
do
	if [[ $found == 0 ]]
	then
		check $i/$1
	else
	echo $found && exit
	fi
done
echo "File not found"
