#!/bin/bash

create()
{
	count=$1
	let "count-=1"
	file=$RANDOM
	if test -f $file
	then
		return
	else
		touch $file
	fi
	if [[ "$count" -ne "0" ]]
	then
		create $count
	else
		exit
	fi
}

if [[ "$1" == "" ]]
then
	echo "Wrong number of arguments" && exit
fi
create $1
