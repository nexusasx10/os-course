@echo off
setlocal enableextensions
set found=0
if "%1"=="" (echo Wrong number of arguments && goto :eof)

for /f "tokens=1" %%i in ('help ^| findstr /b [A-Z] ') do (if "%found%"=="0" (call :checkc %%i %1))

call :check %cd% %1
for %%h in ("%pathext:;=" "%") do (if "%found%"=="0" (call :check %cd% %1 %%h))

for %%i in ("%path:;=" "%") do (if "%found%"=="0" (call :check %%i %1 && for %%j in ("%pathext:;=" "%") do (call :check %%i %1 %%j)))
if "%found%"=="0" (echo File not found && goto :eof)

:check
set full=%~1\%~2%~3
if exist %full:\\=\% (echo %full:\\=\% && set found=1 && goto :eof) else (goto :eof)

:checkc
if /i "%~1"=="%2" (echo Internal command && set found=1 && goto :eof)