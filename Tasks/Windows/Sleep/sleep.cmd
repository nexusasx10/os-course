@echo off
setlocal
set finish=0
if "%1"=="" (echo Wrong number of arguments && goto :eof)
if "%1"=="/?" (type help.txt && goto :eof)
if /i "%1"=="help" (type help.txt && goto :eof)
set arg=%1
set /a argw=%arg:/=%
if "%1"=="0" (echo Argument is zero && goto :eof)
if "%argw%" neq "0" (set /a increment=%1*100 && goto :snd1)

if /i "%1"=="/h" (if "%2" neq "" (set /a increment=%2*360000 && shift && goto :snd) else (echo Wrong number of arguments && goto :eof))
if /i "%1"=="/m" (if "%2" neq "" (set /a increment=%2*6000 && shift && goto :snd) else (echo Wrong number of arguments && goto :eof))
if /i "%1"=="/s" (if "%2" neq "" (set /a increment=%2*100 && shift && goto :snd) else (echo Wrong number of arguments && goto :eof))
if /i "%1"=="/ms" (if "%2" neq "" (set /a increment=%2 && shift && goto :snd) else (echo Wrong number of arguments && goto :eof))
echo Invalid arguments && goto :eof

:snd
set arg=%1
set /a argw=%arg:/=%
if "%1"=="0" (echo Argument is zero && goto :eof)
if "%argw%" == "0" (echo Invalid arguments && goto :eof)
:snd1
if %1 lss 0 (echo Argument is negative && goto :eof)
if "%2" neq "" (echo Wrong number of arguments && goto :eof)

:re
if "%time:~0,1%"=="0" (set /a h=%time:~1,1%*360000) else (set /a h=%time:~0,2%*360000)
if "%time:~3,1%"=="0" (set /a m=%time:~4,1%*6000) else (set /a m=%time:~3,2%*6000)
if "%time:~6,1%"=="0" (set /a s=%time:~7,1%*100) else (set /a s=%time:~6,2%*100)
if "%time:~9,1%"=="0" (set /a ms=%time:~10,1%) else (set /a ms=%time:~9,2%)
if "%finish%"=="0" (set /a finish=%h%+%m%+%s%+%ms%+%increment%)
set /a now=%h%+%m%+%s%+%ms%
if %finish% gtr 8639999 (if "%now%"=="0" (set /a finish-=8640000))
if %now% LEQ %finish% (goto :re)