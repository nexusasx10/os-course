@echo off
setlocal enabledelayedexpansion
if "%1"=="/?" (type help.txt & goto :eof)
if /i "%1"=="help" (type help.txt & goto :eof)

:next
if "%1" neq "" ((set exp=%exp%%1 2>nul || (Echo Incorrect data format && goto :eof)) && shift && goto :next)
:re
set arg=
set /a c+=1
for /f "tokens=%c% delims=+-*/" %%i in ("%exp%") do (set arg=%%i)
if "%arg%"=="" (goto :ok)
if "%arg%"=="  " (goto :ok)
set /a arge="%arg%"
if "%arg%" == "0" goto :re
if "%arg%" == "0  " goto :re
if "%arge%"=="0" (echo Incorrect data format && goto :eof)
goto :re
:ok
set /a res="%exp%" 2>nul || (echo Incorrect data format && goto :eof)

echo %res%