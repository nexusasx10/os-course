@echo off
setlocal enabledelayedexpansion
if not exist output (mkdir output)
for /f "tokens=2 delims=:" %%i in ('chcp') do set charset=%%i
chcp 65001 > nul
set all=82
set complete=0
for /f "tokens=1" %%i in ('help ^| findstr /b [A-Z] ^| findstr /v SC ^| findstr /v GRAFTABL ^| findstr /v DISKPART') do (
if "%%i" neq "For" (
cls
set /a complete+=1
echo !complete!/%all%
help %%i 1>output/%%i.txt 2>nul))
if "%complete%"=="%all%" (echo Completed) else (echo Not completed)
chcp %charset% > nul
pause